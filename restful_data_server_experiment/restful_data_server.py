import os
import string

from flask import Flask, send_file, abort
from flask_restful import Resource, Api
from flask_api import status

data_root_folder = os.path.join(os.getcwd(), "../tests/download/ddd/")

app = Flask(__name__)
api = Api(app)

class GetBinaryFile(Resource):
    """
    Emulates RESTful server that returns binary raw weather data files
    """
    def get(self, id, date, product, format, filetype):
        assert type(id) is int and id >= 0 and id <= 99999, 'Expecting id to be an integer between 0 and 99,999'
        assert type(date) is str and len(date) == 8 and date.isnumeric() is True, 'Expecting date to be a 8 character numeric string in the form of YYYYMMDD'
        assert type(product) is str and product == 'TEMPERATURE' or product == 'WIND', 'Expecting product to be a string with value TEMPERATURE or WIND'
        assert type(format) is str and format == 'ASCII', 'Expecting format to be a string with value ASCII'
        assert type(filetype) is str and filetype == 'ZIP', 'Expecting format to be a string with value ZIP'

        binary_filename = os.path.abspath(os.path.join(data_root_folder, '{:05}'.format(id), date, product, format, filetype, '{}.zip'.format(date)))
        print('Checking for file: {}'.format(binary_filename))
        if os.path.exists(binary_filename):
            print('File exists.  Sending.')
            return send_file(filename_or_fp=binary_filename, mimetype='application/zip', as_attachment=True)
        else:
            print('File does not exist.')
            content = {'File Not Found': 'File Not Found'}
            return None, status.HTTP_500_INTERNAL_SERVER_ERROR

api.add_resource(GetBinaryFile, '/ddd/download/<int:id>/<string:date>/<string:product>/<string:format>/<string:filetype>', endpoint = 'user')

if __name__ == '__main__':
    app.run(debug=True)
    print('Access content at: http://127.0.0.1:5000/ddd/download/12345/20200220/WIND/ASCII/ZIP')