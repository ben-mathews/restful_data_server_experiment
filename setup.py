#!/usr/bin/env python

from distutils.core import setup

setup(
    name="restful_data_server_experiment",
    version="0.0.1",
    description="Package containing modules to experiment with a RESTful data server",
    author="Ben Mathews",
    author_email="beniam@yahoo.com",
    packages=["restful_data_server_experiment"],
    package_data={"restful_data_server_experiment": ["*.txt", "*.json", "./test/*"]},
    include_package_data=True,
    zip_safe=True,
)
