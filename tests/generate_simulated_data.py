"""
Generates a test data file structure for restful_data_server.py
"""
import os
import shutil

ids = [12345, 0, 55555, 99999]
dates = ['20200220', '20200221']
products = ['TEMPERATURE', 'WIND']
formats = ['ASCII']
types = ['ZIP']

root_folder = ''
download_folder = os.path.join(root_folder, 'download')
if os.path.exists(download_folder):
    shutil.rmtree(download_folder)
os.mkdir(download_folder)
data_root_folder = os.path.join(root_folder, 'download', 'ddd')
os.mkdir(data_root_folder)

for id in ids:
    id_folder = os.path.join(data_root_folder, '{:05d}'.format(id))
    os.mkdir(id_folder)
    for date in dates:
        date_folder = os.path.join(id_folder, date)
        os.mkdir(date_folder)
        for product in products:
            product_folder = os.path.join(date_folder, product)
            os.mkdir(product_folder)
            for format in formats:
                format_folder = os.path.join(product_folder, format)
                os.mkdir(format_folder)
                for type in types:
                    type_folder = os.path.join(format_folder, type)
                    os.mkdir(type_folder)
                    output_file = os.path.join(type_folder, '{}.zip'.format(date))
                    with open(output_file, 'wb') as fout:
                        fout.write(os.urandom(1024))  # replace 1024 with size_kb if not unreasonably large
