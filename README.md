# RESTful Data Server Experiment

## Overview

This project is a simple experiment to demonstrate serving up binary data (wind and precipitation data, in this case) over a RESTful interface using Flask.

## Running the Project

The `tests/generate_simulated_data.py` script will generate a tree of simulated binary zip file data.  Run this to generate test data.  Then run `restful_data_server_experiment/restful_data_server.py` to serve this data up.  Clients can access it using URLS structured as `http://127.0.0.1:5000/ddd/download/{id}/{date}/{product}/{format}/{filetype}`.  For example, [http://127.0.0.1:5000/ddd/download/12345/20200220/WIND/ASCII/ZIP](http://127.0.0.1:5000/ddd/download/12345/20200220/WIND/ASCII/ZIP).
